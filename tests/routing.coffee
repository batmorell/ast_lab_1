server = require '../src/server/index.coffee'
request = require 'request'
should = require 'should'

describe 'routing', () ->
  before(() ->
    server.listen(8888);
  );

  it('should return 404', (done) ->
  request.get('http://localhost:8888', (err, res) ->
      res.statusCode.should.equal(404)
      done()
    );
  );

  it('should return 200', (done) ->
  request.get('http://localhost:8888/index', (err, res) ->
      res.statusCode.should.equal(200)
      done()
    );
  );

  after(() ->
    server.close();
  );
