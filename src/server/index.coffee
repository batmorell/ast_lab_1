# Import a module
http = require 'http'
server = require './server.coffee'

# Declare an http server
server = module.exports = http.createServer(server.logic).listen server.port, server.address
